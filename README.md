# Informe de permisos

### Introducción 

##### Objetivos

Conocer que actividades desempeña cada rol dentro del repositorio de otro usuario de Gitlab.

##### ¿Qué es Gitlab?

Gitlab para mi es un cliente del servicio de Git el cual nos ayuda a nosotros a tener un mejor control dentro de nuestro versionamiento.
Ya que nos permite a nosotros poder tener un feedback de todos los colaboradores e incluso ir más allá y poder trabajar con cada uno de ellos de esta manera no solo compartimos la información sino que al mismo tiempo podemos tener la colaboración de varios usuarios.
Teniendo la posibilidad de guardar todos los cambios existentes en caso de que se realicen cambios indebidos.

##### Información del repositorio

En este repositorio nosotros vamos a poder encontrar una lista de las actividades que puede generar un colaborador en diferentes roles dentro de un repositorio externo.

# Analisis

### Rol Guest

![](./imagenes/roll-guest.png)

|Aspecto|Si|No|
|-------|----|----|
|Visualizar la lista de issues|x| |
|Crear issues|x| |
|Editar issues creados por el propietario| |x|
|Editar issues creados por el colaborador|x| |
|Asignar issues| |x|
|Cerrar issues creados por el propietario| |x|
|Cerrar issues creados por el colaborador|x| |
|Crear etiquetas| |x|
|Agregar etiquetas en issues| |x|

### Rol Reporter

![](./imagenes/roll-reporter.png)

|Aspecto|Si|No|
|-------|----|----|
|Visualizar la lista de issues|x| |
|Crear issues|x| |
|Editar issues creados por el propietario|x| |
|Editar issues creados por el colaborador|x| |
|Asignar issues|x| |
|Cerrar issues creados por el propietario|x| |
|Cerrar issues creados por el colaborador|x| |
|Crear etiquetas|x| |
|Agregar etiquetas en issues|x| |

### Rol Developer

![](./imagenes/roll-developer.png)

|Aspecto|Si|No|
|-------|----|----|
|Visualizar la lista de issues|x| |
|Crear issues|x| |
|Editar issues creados por el propietario|x| |
|Editar issues creados por el colaborador|x| |
|Asignar issues|x| |
|Cerrar issues creados por el propietario|x| |
|Cerrar issues creados por el colaborador|x| |
|Crear etiquetas|x| |
|Agregar etiquetas en issues|x| |

### Rol Maintainer

![](./imagenes/roll-maintainer.png)

|Aspecto|Si|No|
|-------|----|----|
|Visualizar la lista de issues|x| |
|Crear issues|x| |
|Editar issues creados por el propietario|x| |
|Editar issues creados por el colaborador|x| |
|Asignar issues|x| |
|Cerrar issues creados por el propietario|x| |
|Cerrar issues creados por el colaborador|x| |
|Crear etiquetas|x| |
|Agregar etiquetas en issues|x| |


# Conclusión

Como pudimos apreciar en el desarrollo del informe cada uno de los roles tienen diferentes posibilidades de interactuar con el repositorio dependiendo del tipo de roll en el que se encuentre.
Siendo de esta manera el rol "guest" el más delimitado ya que como invitado no puede generar algunos cambios al menos en el apartado analizado el cual fue el apartado de Issues.
Y de la misma manera el que tiene mayor privilegio dentro del repositorio es el rol de "maintainer".